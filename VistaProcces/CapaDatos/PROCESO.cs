//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CapaDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class PROCESO
    {
        public PROCESO()
        {
            this.FUNCION_PROCESO = new HashSet<FUNCION_PROCESO>();
            this.DOCUMENTO = new HashSet<DOCUMENTO>();
        }
    
        public decimal ID_PROCESO { get; set; }
        public decimal FUNCIONARIO_ID_FUNCIONARIO { get; set; }
        public string NOMBRE { get; set; }
        public decimal DEPARTAMENTO_DEPARTAMENTO_ID { get; set; }
    
        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }
        public virtual ICollection<FUNCION_PROCESO> FUNCION_PROCESO { get; set; }
        public virtual FUNCIONARIO FUNCIONARIO { get; set; }
        public virtual ICollection<DOCUMENTO> DOCUMENTO { get; set; }
    }
}
