//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CapaDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class DEPARTAMENTO
    {
        public DEPARTAMENTO()
        {
            this.PROCESO = new HashSet<PROCESO>();
        }
    
        public decimal ID_UNIDAD { get; set; }
        public string DESCRIPCION { get; set; }
        public decimal DEPARTAMENTO_ID { get; set; }
    
        public virtual ICollection<PROCESO> PROCESO { get; set; }
    }
}
