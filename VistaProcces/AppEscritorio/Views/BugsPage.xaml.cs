﻿using MahApps.Metro.IconPacks;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using AppEscritorio.ViewModels;
using MenuItem = AppEscritorio.ViewModels.MenuItem;

namespace AppEscritorio.Views
{
    /// <summary>
    /// Lógica de interacción para BugsPage.xaml
    /// </summary>
    public partial class BugsPage : Page
    {

        private static readonly ObservableCollection<MenuItem> AppMenu = new ObservableCollection<MenuItem>();
        private static readonly ObservableCollection<MenuItem> AppOptionsMenu = new ObservableCollection<MenuItem>();

        public ObservableCollection<MenuItem> Menu => AppMenu;

        public ObservableCollection<MenuItem> OptionsMenu => AppOptionsMenu;

        public BugsPage()
        {
            InitializeComponent();
        }
        public void Tile_Click(object sender, RoutedEventArgs e)
        {
            /*ShellViewModel s = new ShellViewModel();
            AgregarContrato ag = new AgregarContrato();
            s.Menu.Add(new MenuItem()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.InfoCircleSolid },
                Label = "Acerca",
                NavigationType = typeof(AgregarContrato),
                NavigationDestination = new Uri("Views/AgregarContrato.xaml", UriKind.RelativeOrAbsolute),
            });*/

            AgregarUsuario au = new AgregarUsuario();

            au.Show();


        }
    }
}
        