﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppEscritorio.Views
{
    /// <summary>
    /// Lógica de interacción para SobreCarga.xaml
    /// </summary>
    public partial class SobreCarga : Page
    {
        public SobreCarga()
        {
            InitializeComponent();
        }

        private void Trafico_IP(object sender, RoutedEventArgs e)
        {
            Rendimiento r = new Rendimiento();
            this.NavigationService.Navigate(r);
        }

        private void Conexiones_Maliciosas(object sender, RoutedEventArgs e)
        {
            ConexionesMaliciosas cm = new ConexionesMaliciosas();
            this.NavigationService.Navigate(cm);
        }

        private void Boton_Flujo(object sender, RoutedEventArgs e)
        {
            Flujo f = new Flujo();
            this.NavigationService.Navigate(f);
        }
    }
}
