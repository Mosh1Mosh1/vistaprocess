﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppEscritorio.Views
{
    /// <summary>
    /// Lógica de interacción para Flujo.xaml
    /// </summary>
    public partial class Flujo : Page
    {
        public Flujo()
        {
            InitializeComponent();
        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
            CrearFlujo cf = new CrearFlujo();

            cf.Show();
        }

        private void Boton_Trafico_IP(object sender, RoutedEventArgs e)
        {
            Rendimiento r = new Rendimiento();
            this.NavigationService.Navigate(r);
        }

        private void Boton_ConexionesMaliciosas(object sender, RoutedEventArgs e)
        {
            SobreCarga sc = new SobreCarga();
            this.NavigationService.Navigate(sc);
        }

        private void Boton_SobreCarga(object sender, RoutedEventArgs e)
        {
            SobreCarga sc = new SobreCarga();
            this.NavigationService.Navigate(sc);
        }
    }
}
