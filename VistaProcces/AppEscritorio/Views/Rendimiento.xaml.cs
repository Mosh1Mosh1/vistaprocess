﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppEscritorio.Views
{
    /// <summary>
    /// Lógica de interacción para AwesomePage.xaml
    /// </summary>
    public partial class Rendimiento : Page
    {
        public Rendimiento()
        {
            InitializeComponent();
        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
            AgregarUnidad au = new AgregarUnidad();

            au.Show();
        }

        private void Tile_Click_1(object sender, RoutedEventArgs e)
        {
            ModificarUnidad mu = new ModificarUnidad();

            mu.Show();
        }
        

        private void Boton_SobreCarga(object sender, RoutedEventArgs e)
        {
            SobreCarga sc = new SobreCarga();
            this.NavigationService.Navigate(sc);

        }
        private void Boton_Flujo(object sender, RoutedEventArgs e)
        {
            Flujo f = new Flujo();
            this.NavigationService.Navigate(f);

        }
        private void Boton_ConexionesMaliciosas(object sender, RoutedEventArgs e)
        {

            ConexionesMaliciosas cm = new ConexionesMaliciosas();
            this.NavigationService.Navigate(cm);

        }
    }
}
