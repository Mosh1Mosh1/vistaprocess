﻿using AppEscritorio.Mvvm;
using AppEscritorio.Views;
using MahApps.Metro.IconPacks;
using System;
using System.Collections.ObjectModel;

namespace AppEscritorio.ViewModels
{
    
        public class ShellViewModelEjecutivo : BindableBase
        {
            private static readonly ObservableCollection<MenuItem> AppMenu = new ObservableCollection<MenuItem>();
            private static readonly ObservableCollection<MenuItem> AppOptionsMenu = new ObservableCollection<MenuItem>();

            public ObservableCollection<MenuItem> Menu => AppMenu;

            public ObservableCollection<MenuItem> OptionsMenu => AppOptionsMenu;

            public ShellViewModelEjecutivo()
            {
                // Build the menus
                /*this.Menu.Add(new MenuItem()
                {
                    Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.FileContractSolid },
                    Label = "Contrato",
                    NavigationType = typeof(BugsPage),
                    NavigationDestination = new Uri("Views/BugsPage.xaml", UriKind.RelativeOrAbsolute)
                });*/
                this.Menu.Add(new MenuItem()
                {
                    Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.HomeSolid },
                    Label = "Inicio",
                    NavigationType = typeof(UserPage),
                    NavigationDestination = new Uri("Views/MainPage.xaml", UriKind.RelativeOrAbsolute)
                });
                this.Menu.Add(new MenuItem()
                {
                    Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.UserTieSolid },
                    Label = "Contrato",
                    NavigationType = typeof(BreakPage),
                    NavigationDestination = new Uri("Views/Contrato.xaml", UriKind.RelativeOrAbsolute)
                });
                /*this.Menu.Add(new MenuItem()
                {
                    Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.BriefcaseSolid },
                    Label = "Rendimiento",
                    NavigationType = typeof(Rendimiento),
                    NavigationDestination = new Uri("Views/Rendimiento.xaml", UriKind.RelativeOrAbsolute)
                });*/

                /*this.OptionsMenu.Add(new MenuItem()
                {
                    Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.CogsSolid },
                    Label = "Configuracion",
                    NavigationType = typeof(SettingsPage),
                    NavigationDestination = new Uri("Views/SettingsPage.xaml", UriKind.RelativeOrAbsolute)
                });*/
                this.OptionsMenu.Add(new MenuItem()
                {
                    Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.InfoCircleSolid },
                    Label = "Acerca",
                    NavigationType = typeof(AboutPage),
                    NavigationDestination = new Uri("Views/AboutPage.xaml", UriKind.RelativeOrAbsolute)
                });
            }
        }
    
}
