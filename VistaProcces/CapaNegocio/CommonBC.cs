﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;

namespace CapaNegocio
{
    public class CommonBC
    {
        private static ProccesEntities modeloEntities;

        public static ProccesEntities ModeloEntities { 
            get
            {
                if (modeloEntities == null)
                {
                    modeloEntities = new ProccesEntities();
                }
                return modeloEntities;
            } 
             
        }
    }
}
