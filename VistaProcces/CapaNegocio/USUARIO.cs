﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class USUARIO
    {
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public int ROL_ID_ROL { get; set; }
        public USUARIO()
        {

        }
            
        public USUARIO(string username, string password, int rol_id_rol)
        {
            USERNAME = username;
            PASSWORD = password;
            ROL_ID_ROL = rol_id_rol;
        }

        public bool NInsertar()
        {
            try
            {
                CommonBC.ModeloEntities.pl_insertarusuario(USERNAME, PASSWORD, ROL_ID_ROL);
                CommonBC.ModeloEntities.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /*public bool NAccederLogin()
        {
            try
            {
                CommonBC.ModeloEntities.pl_insertarusuario(USERNAME, PASSWORD);
                CommonBC.ModeloEntities.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
            



            
        }*/
    }
}
